package com.kk.repositories;

import com.kk.models.Country;
import com.kk.models.IsOfficial;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class CountryRepository extends GenericDao<Country, String> {
    public CountryRepository(EntityManager entityManager) {
        super(entityManager);
    }

    public List<Country> findAll() {
        TypedQuery<Country> query = em.createNamedQuery("Country.findAll", Country.class);
        return query.getResultList();
    }

    public Country findTheBiggest() {
        TypedQuery<Country> query = em.createNamedQuery("Country.findTheBiggest", Country.class);
        return query.getResultList().get(0);
    }

    public Double findAvgLifeExpectancyInEurope() {
        TypedQuery<Double> query = em.createNamedQuery("Country.findAvgLifeExpectancyInEurope", Double.class);
        return query.getSingleResult();
    }

    public List<Country> findAllWithOfficialEnglish() {
        TypedQuery<Country> query = em.createNamedQuery("Country.findAllWithOfficialEnglish", Country.class);
        query.setParameter("b", IsOfficial.F);
        return query.getResultList();

    }

    public List<Country> getCountriesForPop(Long minRange, Long maxRange) {
        String query = "SELECT c FROM Country c WHERE c.population > :min AND c.population < :max ORDER by c.population";
        TypedQuery<Country> typedQuery = em.createQuery(query, Country.class);
        typedQuery.setParameter("min", minRange);
        typedQuery.setParameter("max", maxRange);
        return typedQuery.getResultList();
    }

    public List<Country> getCountriesForLang(String lang) {
        String query = "SELECT c FROM Country c INNER JOIN c.countryLanguage cl WHERE cl.language = :language";
        TypedQuery<Country> typedQuery = em.createQuery(query, Country.class);
        typedQuery.setParameter("language", lang);
        return typedQuery.getResultList();
    }

    public Country getCountryforCity(String city) {
        String query = "SELECT c FROM Country c INNER JOIN c.city city WHERE city.name = :city";
        TypedQuery<Country> typedQuery = em.createQuery(query, Country.class);
        typedQuery.setParameter("city", city);
        return typedQuery.getSingleResult();
    }

    public List<Country> getCountriesFromRange(int first, int last) {
        String query = "SELECT c FROM Country c";
        TypedQuery<Country> typedQuery = em.createQuery(query, Country.class);
        typedQuery.setFirstResult(first);
        typedQuery.setMaxResults(last);
        return typedQuery.getResultList();
    }


}
