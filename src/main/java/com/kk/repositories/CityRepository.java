package com.kk.repositories;

import com.kk.models.City;
import com.kk.models.Country;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

public class CityRepository extends GenericDao<City, Long> {
    public CityRepository(EntityManager entityManager) {
        super(entityManager);
    }

    public List<City> findAllInPoland(){
        TypedQuery<City> query = em.createNamedQuery("City.findAllInPoland", City.class);
        query.setParameter("country", "Poland");
        return query.getResultList();
    }

    public Double findAvgPopulationInLesserPoland(){
        TypedQuery<Double> query = em.createNamedQuery("City.findAvgPopulationInLesserPoland", Double.class);
        query.setParameter("district", "Malopolskie");
        return query.getSingleResult();
    }

    public List<City> filterByDistrict(String district) {
        String query = "SELECT c FROM City c  WHERE c.district = :district";
        TypedQuery<City> typedQuery = em.createQuery(query, City.class);
        typedQuery.setParameter("district", district);
        return typedQuery.getResultList();
    }

    public List<City> filterByCountry(Country country) {
        String query = "SELECT c FROM City c INNER JOIN c.countryCode cc WHERE cc.name = :country";
        TypedQuery<City> typedQuery = em.createQuery(query, City.class);
        typedQuery.setParameter("country", country.getName());
        return typedQuery.getResultList();
    }

    public List<City> filterByLang(String lang) {
        String query = "SELECT c FROM City c INNER JOIN c.countryCode cc INNER JOIN cc.countryLanguage cl WHERE cl.language = :language";
        TypedQuery<City> typedQuery = em.createQuery(query, City.class);
        typedQuery.setParameter("language", lang);
        return typedQuery.getResultList();

    }
}
