package com.kk.models;

import javax.persistence.*;

@Entity
@Table(name = "countrylanguage")
@IdClass(CountryLanguageKeyId.class)
public class CountryLanguage {

    @Id
    private String language;

    @Id
    @ManyToOne
    @JoinColumn(name = "countryCode")
    private Country countryCode;

    @Column(nullable = false)
    private IsOfficial isOfficial;

    @Column(nullable = false)
    private Float precentage;
}
