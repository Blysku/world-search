package com.kk.models;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "country")
@NamedQueries({
        @NamedQuery(name = "Country.findAll", query = "SELECT c FROM Country c"),
        @NamedQuery(name = "Country.findTheBiggest", query = "SELECT c FROM Country c ORDER BY c.surfaceArea DESC"),
        @NamedQuery(name = "Country.findAvgLifeExpectancyInEurope", query = "SELECT AVG(c.lifeExpectancy) FROM Country c"),
        @NamedQuery(name = "Country.findAllWithOfficialEnglish", query = "SELECT c FROM Country c INNER JOIN c.countryLanguage cl WHERE cl.isOfficial = :b")
})
public class Country implements Serializable {

    @Id
    @Column(length = 3, nullable = false)
    private String code;

    @Column(nullable = false, length = 52)
    private String name;

    @Column(nullable = false)
    private String continent;

    @Column(nullable = false, length = 26)
    private String region;

    @Column(nullable = false)
    private Double surfaceArea;

    private Integer indepYear;

    @Column(nullable = false)
    private Long population;

    private Double lifeExpectancy;

    private Double gNP;

    private Double gNPOld;

    @Column(nullable = false)
    private String localName;

    @Column(nullable = false)
    private String governmentForm;

    @Column(nullable = false)
    private String headOfState;

    private String capital;

    private String code2;

    @OneToMany(mappedBy = "countryCode")
    private List<City> city;

    @OneToMany(mappedBy = "countryCode")
    private List<CountryLanguage> countryLanguage;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Country country = (Country) o;
        return Objects.equals(code, country.code) &&
                Objects.equals(name, country.name) &&
                Objects.equals(continent, country.continent) &&
                Objects.equals(region, country.region) &&
                Objects.equals(surfaceArea, country.surfaceArea) &&
                Objects.equals(indepYear, country.indepYear) &&
                Objects.equals(population, country.population) &&
                Objects.equals(lifeExpectancy, country.lifeExpectancy) &&
                Objects.equals(gNP, country.gNP) &&
                Objects.equals(gNPOld, country.gNPOld) &&
                Objects.equals(localName, country.localName) &&
                Objects.equals(governmentForm, country.governmentForm) &&
                Objects.equals(headOfState, country.headOfState) &&
                Objects.equals(capital, country.capital) &&
                Objects.equals(code2, country.code2) &&
                Objects.equals(city, country.city) &&
                Objects.equals(countryLanguage, country.countryLanguage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(code, name, continent, region, surfaceArea, indepYear, population, lifeExpectancy, gNP, gNPOld, localName, governmentForm, headOfState, capital, code2, city, countryLanguage);
    }

    @Override
    public String toString() {
        return "Country{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                ", continent='" + continent + '\'' +
                ", region='" + region + '\'' +
                ", surfaceArea=" + surfaceArea +
                ", indepYear=" + indepYear +
                ", population=" + population +
                ", lifeExpectancy=" + lifeExpectancy +
                ", gNP=" + gNP +
                ", gNPOld=" + gNPOld +
                ", localName='" + localName + '\'' +
                ", governmentForm='" + governmentForm + '\'' +
                ", headOfState='" + headOfState + '\'' +
                ", capital='" + capital + '\'' +
                ", code2='" + code2 + '\'' +
                '}';
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getContinent() {
        return continent;
    }

    public void setContinent(String continent) {
        this.continent = continent;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public Double getSurfaceArea() {
        return surfaceArea;
    }

    public void setSurfaceArea(Double surfaceArea) {
        this.surfaceArea = surfaceArea;
    }

    public Integer getIndepYear() {
        return indepYear;
    }

    public void setIndepYear(Integer indepYear) {
        this.indepYear = indepYear;
    }

    public Long getPopulation() {
        return population;
    }

    public void setPopulation(Long population) {
        this.population = population;
    }

    public Double getLifeExpectancy() {
        return lifeExpectancy;
    }

    public void setLifeExpectancy(Double lifeExpectancy) {
        this.lifeExpectancy = lifeExpectancy;
    }

    public Double getgNP() {
        return gNP;
    }

    public void setgNP(Double gNP) {
        this.gNP = gNP;
    }

    public Double getgNPOld() {
        return gNPOld;
    }

    public void setgNPOld(Double gNPOld) {
        this.gNPOld = gNPOld;
    }

    public String getLocalName() {
        return localName;
    }

    public void setLocalName(String localName) {
        this.localName = localName;
    }

    public String getGovernmentForm() {
        return governmentForm;
    }

    public void setGovernmentForm(String governmentForm) {
        this.governmentForm = governmentForm;
    }

    public String getHeadOfState() {
        return headOfState;
    }

    public void setHeadOfState(String headOfState) {
        this.headOfState = headOfState;
    }

    public String getCapital() {
        return capital;
    }

    public void setCapital(String capital) {
        this.capital = capital;
    }

    public String getCode2() {
        return code2;
    }

    public void setCode2(String code2) {
        this.code2 = code2;
    }

    public List<City> getCity() {
        return city;
    }

    public void setCity(List<City> city) {
        this.city = city;
    }

    public List<CountryLanguage> getCountryLanguage() {
        return countryLanguage;
    }

    public void setCountryLanguage(List<CountryLanguage> countryLanguage) {
        this.countryLanguage = countryLanguage;
    }
}

