package com.kk.app;

import com.kk.models.City;
import com.kk.models.Country;
import com.kk.repositories.CityRepository;
import com.kk.repositories.CountryRepository;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

public class main {
    public static void main(String[] args) {

        EntityManagerFactory managerFactory = Persistence.createEntityManagerFactory("worldsearch");
        EntityManager entityManager = managerFactory.createEntityManager();
        CountryRepository countryRepository = new CountryRepository(entityManager);
        List<Country> countriesForPop = countryRepository.getCountriesForPop(100000L, 1000000L);
        for (Country c : countriesForPop) {
            System.out.println(c);
        }

        separator();

        List<Country> countriesForLang = countryRepository.getCountriesForLang("English");

        for (Country c : countriesForLang) {
            System.out.println(c);
        }

        separator();

        System.out.println(countryRepository.getCountryforCity("Aba"));

        separator();

        List<Country> countriesFromRange = countryRepository.getCountriesFromRange(1,5);

        for (Country c : countriesFromRange) {
            System.out.println(c);
        }

        separator();

        CityRepository cityRepository = new CityRepository(entityManager);

        List<City> citiesByDistrict = cityRepository.filterByDistrict("Adana");

        for (City c : citiesByDistrict) {
            System.out.println(c);
        }

        separator();

        List<City> citiesByCountry = cityRepository.filterByCountry(countriesForLang.get(1));

        for (City c : citiesByCountry) {
            System.out.println(c);
        }

        separator();

        List<City> citiesByLang = cityRepository.filterByLang("English");

        for (City c : citiesByLang) {
            System.out.println(c);
        }

        separator();

        List<Country> allCountries = countryRepository.findAll();

        for (Country c : allCountries) {
            System.out.println(c);
        }

        separator();

        System.out.println(countryRepository.findTheBiggest());

        separator();

        System.out.println(countryRepository.findAvgLifeExpectancyInEurope());

        separator();

        List<Country> countryEnglish = countryRepository.findAllWithOfficialEnglish();

        for (Country c : allCountries) {
            System.out.println(c);
        }

        separator();

        List<City> allCities = cityRepository.findAllInPoland();

        for (City c : allCities) {
            System.out.println(c);
        }

        separator();

        System.out.println(cityRepository.findAvgPopulationInLesserPoland());

        entityManager.close();
    }

    private static void separator() {
        for (int i = 0; i < 10; i++) {
            System.out.println("=========================================================");
        }
    }
}
